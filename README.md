![icon.png](https://bitbucket.org/repo/zAKkBy/images/278640877-icon.png)

# RiftMediaPlayer - VR media player for Oculus Rift #

The RiftMediaPlayer is a thesis project started as an experimental project in my spare time and soon become the player used by Imille s.r.l. in many projects with customers like Pandistelle and Corona. It's based on the LibAV and can decode MP4 streams with h264 codec and pixel format of YUV420P with an optional stereo audio stream at 48KHz. It's pretty basic to keep it very simple. The player uses OpenGL as a rendering API and it supports the DirectMode for the Oculus Rift. Unluckily OpenGL is not well supported with the Oculus SDK so I've managed to build another player from scratch that basically uses Direct3D as a main renderer. The omiPlayer is a barebone player that can be found at https://bitbucket.org/omigamedev/omiplayer

This is the first versione of the UI. A basic interface with a timeline and simple self explanatory buttons.

![rmp2_1.png](https://bitbucket.org/repo/zAKkBy/images/2330614929-rmp2_1.png)

This is how it looks with a bigger font and some markers used in the Pandistelle event by the crew.

![rmp3_1.png](https://bitbucket.org/repo/zAKkBy/images/2498753161-rmp3_1.png)

This is basically the viewport in Windowed mode, the DirectMode doesn't have a mirror (for performance reasons)

![rmp1_1.png](https://bitbucket.org/repo/zAKkBy/images/3312920353-rmp1_1.png)