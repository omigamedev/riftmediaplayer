@echo off
SET st2Path=%CD%\RiftMediaPlayer.exe

rem associate .rmp file to %st2Path%
@reg add "HKEY_CLASSES_ROOT\.rmp" /t REG_SZ /v "" /d "RiftMediaPlayer" /f
@reg add "HKEY_CLASSES_ROOT\.rmp\DefaultIcon" /t REG_SZ /v "" /d "%st2Path%,0" /f
@reg add "HKEY_CLASSES_ROOT\RiftMediaPlayer\Shell\Open\Command" /t REG_EXPAND_SZ /v "" /d "%st2Path% \"%%1\"" /f

pause