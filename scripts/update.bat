@echo off

set SRC=%~dp0
set DEST=C:\Program Files\RiftMediaPlayer\

echo updating %SRC% into %DEST%
xcopy /Y "%SRC%*.exe" "%DEST%"
xcopy /YS "%SRC%shaders\*" "%DEST%shaders\"

pause
