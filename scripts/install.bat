@echo off

set SRC=%~dp0
set DEST=C:\Program Files\RiftMediaPlayer\

echo Installing %SRC% into %DEST%
xcopy /Y "%SRC%*.bat" "%DEST%"
xcopy /Y "%SRC%*.dll" "%DEST%"
xcopy /Y "%SRC%*.exe" "%DEST%"
xcopy /YS "%SRC%shaders\*" "%DEST%shaders\"

echo Creating templates in %Systemroot%\ShellNew
xcopy /Y "%SRC%templates\*" "%Systemroot%\ShellNew\"

SET st2Path=%DEST%RiftMediaPlayer.exe
echo Register file association .rmp file to %st2Path%
@reg add "HKEY_CLASSES_ROOT\.rmp" /t REG_SZ /v "" /d "RiftMediaPlayer" /f
@reg add "HKEY_CLASSES_ROOT\.rmp\DefaultIcon" /t REG_SZ /v "" /d "%st2Path%,0" /f
@reg add "HKEY_CLASSES_ROOT\RiftMediaPlayer\Shell\Open\Command" /t REG_EXPAND_SZ /v "" /d "%st2Path% \"%%1\"" /f
@reg add "HKEY_CLASSES_ROOT\.rmp\ShellNew" /t REG_SZ /v "Filename" /d "RMP-Bottom-up.rmp" /f
echo Register OpenWith RiftMediaPlayer shell extension
@reg add "HKEY_CLASSES_ROOT\*\shell\Open with RiftMediaPlayer"         /t REG_SZ /v "" /d "Open with RiftMediaPlayer"   /f
@reg add "HKEY_CLASSES_ROOT\*\shell\Open with RiftMediaPlayer"         /t REG_EXPAND_SZ /v "Icon" /d "%st2Path%,0" /f
@reg add "HKEY_CLASSES_ROOT\*\shell\Open with RiftMediaPlayer\command" /t REG_SZ /v "" /d "%st2Path% \"%%1\"" /f

pause
