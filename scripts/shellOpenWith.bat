@echo off
SET st2Path=%CD%\RiftMediaPlayer.exe
 
rem add it for all file types
@reg add "HKEY_CLASSES_ROOT\*\shell\Open with RiftMediaPlayer"         /t REG_SZ /v "" /d "Open with RiftMediaPlayer"   /f
@reg add "HKEY_CLASSES_ROOT\*\shell\Open with RiftMediaPlayer"         /t REG_EXPAND_SZ /v "Icon" /d "%st2Path%,0" /f
@reg add "HKEY_CLASSES_ROOT\*\shell\Open with RiftMediaPlayer\command" /t REG_SZ /v "" /d "%st2Path% \"%%1\"" /f

pause