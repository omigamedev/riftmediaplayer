uniform sampler2D tex0;
varying float vignette;

void main()
{
    vec3 color;
    color.r = texture2D(tex0, gl_TexCoord[0].st).r;
    color.g = texture2D(tex0, gl_TexCoord[1].st).g;
    color.b = texture2D(tex0, gl_TexCoord[2].st).b;
    gl_FragColor = vec4(color * vignette, 1);
}
