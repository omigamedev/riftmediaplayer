#version 120
uniform vec2 texScale;
uniform vec2 texOffset;
uniform bool chromaEnabled;
attribute float attVignette;
attribute vec2 attTexR;
attribute vec2 attTexG;
attribute vec2 attTexB;
varying float vignette;

void main()
{
    gl_Position = gl_Vertex;
    if (chromaEnabled)
    {
        gl_TexCoord[0].st = attTexR * texScale + texOffset;
        gl_TexCoord[1].st = attTexG * texScale + texOffset;
        gl_TexCoord[2].st = attTexB * texScale + texOffset;
    }
    else
    {
        gl_TexCoord[0].st = attTexB * texScale + texOffset;
        gl_TexCoord[1].st = attTexB * texScale + texOffset;
        gl_TexCoord[2].st = attTexB * texScale + texOffset;
    }
    vignette = attVignette;
}
