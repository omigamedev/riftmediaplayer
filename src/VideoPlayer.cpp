#include "stdafx.h"
#include "VideoPlayer.h"


VideoPlayer::VideoPlayer()
{
    format = nullptr;

    video_decoder = nullptr;
    video_context = nullptr;
    video_stream = nullptr;
    video_convert_ctx = nullptr;
    video_frame = nullptr;
    video_stream_index = -1;
    video_tex = 0;
    video_pbo_last = 0;
    rgb_size = 0;
    width = 0;
    height = 0;
    video_current_pbo = 0;

    audio_decoder = nullptr;
    audio_context = nullptr;
    audio_stream = nullptr;
    audio_frame = nullptr;
    audio_resample = nullptr;
    audio_stream_index = -1;
    audio_source = 0;

    active = false;
}


VideoPlayer::~VideoPlayer()
{
    close();
}


bool VideoPlayer::load(const char* filename, bool ignore_audio)
{
    path = filename;

    // Open file and read streams info
    if (avformat_open_input(&format, filename, nullptr, nullptr) < 0)
        return false;
    if (avformat_find_stream_info(format, nullptr) < 0)
        return false;

    // Open video stream
    video_stream_index = av_find_best_stream(format,
        AVMEDIA_TYPE_VIDEO, -1, -1, &video_decoder, 0);
    if (video_stream_index >= 0)
    {
        video_stream = format->streams[video_stream_index];
        video_context = video_stream->codec;
        if (avcodec_open2(video_context, video_decoder, nullptr) == 0)
        {
            width = video_context->width;
            width2 = width / 2;
            height = video_context->height;
            height2 = height / 2;

            // Setup video buffers
            video_frame = av_frame_alloc();
            video_frame->format = video_context->pix_fmt;
            video_frame->width = width;
            video_frame->height = height;
            av_frame_get_buffer(video_frame, 0);
            avcodec_default_get_buffer2(video_context, video_frame, AV_GET_BUFFER_FLAG_REF);

            rgb_size = width * (height+height2);
        }
        else
        {
            printf("Unable to open the video codec.\n");
            video_stream_index = -1;
            return 0;
        }
    }
    else
    {
        printf("Video stream not found.\n");
        video_stream_index = -1;
        return 0;
    }

    // Open audio stream
    audio_stream_index = av_find_best_stream(format,
        AVMEDIA_TYPE_AUDIO, -1, -1, &audio_decoder, 0);
    if (audio_stream_index >= 0 && !VP_IGNORE_AUDIO && !ignore_audio)
    {
        audio_stream = format->streams[audio_stream_index];
        audio_context = audio_stream->codec;
        if (avcodec_open2(audio_context, audio_decoder, nullptr) == 0)
        {
            // Setup buffer
            audio_frame = av_frame_alloc();

            // Setup audio resampler
            audio_resample = avresample_alloc_context();
            av_opt_set_int(audio_resample, "in_channel_layout", audio_context->channel_layout, 0);
            av_opt_set_int(audio_resample, "in_sample_rate", audio_context->sample_rate, 0);
            av_opt_set_int(audio_resample, "in_sample_fmt", audio_context->sample_fmt, 0);
            av_opt_set_int(audio_resample, "out_channel_layout", audio_context->channel_layout, 0);
            av_opt_set_int(audio_resample, "out_sample_rate", audio_context->sample_rate, 0);
            av_opt_set_int(audio_resample, "out_sample_fmt", AV_SAMPLE_FMT_S16, 0);
            if (avresample_open(audio_resample) != 0)
            {
                printf("Cannot open audio resampler.\n");
                audio_stream_index = -1;
            }

            // Create OpenAL buffers
            alGenSources(1, &audio_source);
        }
        else
        {
            printf("Unable to open the audio codec.\n");
            audio_stream_index = -1;
        }
    }
    else
    {
        printf("No audio track found.\n");
        audio_stream_index = -1;
    }

    video_pbo_last = 0;
    active = true;
    read_thread = std::thread(&VideoPlayer::decodeThread, this);

    {
        std::unique_lock<std::mutex> lock(read_mutex);
        read_cv.wait(lock);
    }

    if (onLoad)
        onLoad();

    return true;
}

void VideoPlayer::decodeFrame(ID3D11DeviceContext* ctx, ID3D11Texture2D* tex)
{
    static AVPacket packet;
    static int rewinded = 0;
    int decoded_video = 0;
    int decoded_audio = 0;

    if (!format)
        return;

    std::unique_lock<std::mutex> lock(read_mutex);
    read_cv.wait(lock, [&]() {
        if (video_pbos_empty.size() > 0 || !active)
        {
            return true; // Stop waiting
        }
        else
        {
            return false; // No buffers available, wait
        }
    });

    if (!active)
        return;

    // Get an empty PBO
    GLuint pbo = video_pbos_empty.back();
    video_pbos_empty.pop_back();
    lock.unlock();

    bool audio_need = audio_buffer_queue.size() < 10;
    //while (!(decoded_video && (decoded_audio || !audio_need)))
    while (!decoded_video)
    {
        av_init_packet(&packet);
        av_read_frame(format, &packet);
        if (packet.stream_index == video_stream_index)
        {
            //video_packets.push_front(packet);
            //
            //if (decoded_video)
            //    continue;
            //
            //packet = video_packets.back();
            //video_packets.pop_back();

            int ret = avcodec_decode_video2(video_context, video_frame, &decoded_video, &packet);
            if (ret <= 0 && !decoded_video && !rewinded)
            {
                if (onComplete)
                    onComplete();
                rewind();
                rewinded = 1;
                break;
            }
            if (decoded_video)
            {
                //printf("decodeFrame video frame, queued %d frames\n", video_packets.size());
                rewinded = 0;

                {
                    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo);
                    uint8_t* ptr = (uint8_t*)glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);
                    if (ptr)
                    {
                        // Copy Y plane
                        for (int i = 0; i < height; i++)
                            memcpy(ptr + i*width, video_frame->data[0] + i * video_frame->linesize[0], width);
                        // Copy U and V plane
                        for (int i = 0; i < height2; i++)
                        {
                            memcpy(ptr + (i + height) * width, video_frame->data[1] + i * video_frame->linesize[1], width2);
                            memcpy(ptr + (i + height) * width + width2, video_frame->data[2] + i * video_frame->linesize[2], width2);
                        }
                    }
                    glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
                    //glFlush();
                }


                // Write a ready PBO
                std::unique_lock<std::mutex> lock(read_mutex);
                video_pbos_ready.push_front(pbo);
                lock.unlock();

                av_free_packet(&packet);
            }
        }
        else if (packet.stream_index == audio_stream_index)
        {
            avcodec_decode_audio4(audio_context, audio_frame, &decoded_audio, &packet);
            if (decoded_audio)
            {
                //printf("decodeFrame audio frame\n");

                int lineSize = 0;
                int dataSize = av_samples_get_buffer_size(&lineSize, audio_context->channels,
                    audio_frame->nb_samples, AV_SAMPLE_FMT_S16, 1);

                uint8_t* buffer = (uint8_t*)av_mallocz(dataSize);
                int samples = avresample_convert(audio_resample, &buffer, lineSize, audio_frame->nb_samples,
                    audio_frame->data, audio_frame->linesize[8], audio_frame->nb_samples);

                ALuint albuffer;
                alGenBuffers(1, &albuffer);

                alBufferData(albuffer, AL_FORMAT_STEREO16, buffer, dataSize, audio_frame->sample_rate);
                alSourceQueueBuffers(audio_source, 1, &albuffer);

                std::unique_lock<std::mutex> audio_lock(audio_buffer_mutex);
                audio_buffer_queue.push_front(albuffer);
                audio_lock.unlock();

                av_free_packet(&packet);
                //printf("Decoded audio %d buffer %d samples, queue size %d\n",
                //    dataSize, samples, audio_buffer_queue.size());
            }
        }
    }

    // Notify a frame is ready (maybe)
    read_cv.notify_all();
}

void VideoPlayer::close()
{
    active = false;
    read_cv.notify_all();
    if (read_thread.joinable())
        read_thread.join();
    if (format)
    {
        if (video_stream_index >= 0)
        {
            sws_freeContext(video_convert_ctx);
            av_frame_free(&video_frame);
            avcodec_close(video_context);
        }
        if (audio_stream_index >= 0)
        {
            avresample_free(&audio_resample);
            av_frame_free(&audio_frame);
            avcodec_close(audio_context);
        }
        avformat_close_input(&format);
    }
    format = nullptr;
}

void VideoPlayer::rewind()
{
    if (!format)
        return;
    
    av_seek_frame(format, video_stream_index, 0, AVSEEK_FLAG_BACKWARD);
    
    video_played_frames = 0;
    audio_played_samples = 0;
}

bool VideoPlayer::getNextFrame()
{
    if (!format)
        return false;

    //std::unique_lock<std::mutex> lock(read_mutex);
    
    if (video_pbos_ready.size() == 0)
    {
        printf("no ready frames\n");
        return false;
    }

    GLuint pbo = video_pbos_ready.back();
    video_pbos_ready.pop_back();
    video_pbo_last = pbo;
    //video_pbos_empty.push_front(pbo); // TODO: check
    //lock.unlock();

    if (!active)
        return false;

    read_cv.notify_all();

    // Upload decoded frame to texture data
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, pbo);
    glBindTexture(GL_TEXTURE_2D, video_tex);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height + height2, GL_LUMINANCE, GL_UNSIGNED_BYTE, 0);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);

    video_played_frames++;
    return true;
}

void VideoPlayer::getNextFrame_post()
{
    //std::unique_lock<std::mutex> lock(read_mutex);
    if (!video_pbo_last)
        return;
    video_pbos_empty.push_front(video_pbo_last);
    video_pbo_last = 0;
    //lock.unlock();
    read_cv.notify_all();
}

void VideoPlayer::decodeThread()
{
    std::unique_lock<std::mutex> lock(read_mutex);

    BOOL ret = wglMakeCurrent(GetDC(glfwGetWin32Window(window)), gl_ctx);

    // Create OpenGL video texture
    glGenTextures(1, &video_tex);
    glBindTexture(GL_TEXTURE_2D, video_tex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width, height + height2,
        0, GL_LUMINANCE, GL_UNSIGNED_BYTE, nullptr);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, 0);

    // Create OpenGL PBOs
    GLuint video_pbos[VP_PBOS];
    glGenBuffers(VP_PBOS, video_pbos);
    for (int i = 0; i < VP_PBOS; i++)
    {
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, video_pbos[i]);
        glBufferData(GL_PIXEL_UNPACK_BUFFER, rgb_size, nullptr, GL_STREAM_DRAW_ARB);
        video_pbos_empty.push_front(video_pbos[i]);
    }
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    //glFlush();
    lock.unlock();
    read_cv.notify_all();

    while (active)
    {
        decodeFrame();
    }
    int end = 0;
}

void VideoPlayer::updateAudio()
{
    if (!format || !audio_context)
        return;

    double video_rate = (double)video_stream->avg_frame_rate.den /
        (float)video_stream->avg_frame_rate.num;
    double audio_rate = 1.0 / audio_context->sample_rate;

    double video_time = video_played_frames * video_rate;
    double audio_time = audio_played_samples * audio_rate;

    int val = 0;
    if (video_time > audio_time)
    {
        alGetSourcei(audio_source, AL_SOURCE_STATE, &val);
        if (val != AL_PLAYING)
            alSourcePlay(audio_source);
    }
    else
    {
        alGetSourcei(audio_source, AL_SOURCE_STATE, &val);
        if (val == AL_PLAYING)
            alSourcePause(audio_source);
    }

    std::unique_lock<std::mutex> audio_lock(audio_buffer_mutex);
    alGetSourcei(audio_source, AL_BUFFERS_PROCESSED, &val);
    while (val--)
    {
        alSourceUnqueueBuffers(audio_source, 1, &audio_buffer_queue.back());
        alDeleteBuffers(1, &audio_buffer_queue.back());
        audio_buffer_queue.pop_back();
        audio_played_samples += audio_context->frame_size;
    }
    if (audio_buffer_queue.size() == 0)
    {
        printf("Zero audio\n");
    }
    audio_lock.unlock();
}

float VideoPlayer::getProgress()
{
    if (!format)
        return 0;
    double factor = (double)video_stream->time_base.den / (double)video_stream->time_base.num;
    double duration = video_stream->duration / factor;

    double video_rate = (double)video_stream->avg_frame_rate.den /
        (float)video_stream->avg_frame_rate.num;
    double video_time = video_played_frames * video_rate;

    return (float)(video_time / duration);
}
