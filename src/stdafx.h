#pragma once

#define _CRT_SECURE_NO_WARNINGS
#define _USE_MATH_DEFINES
#define GLM_FORCE_RADIANS
#define GLFW_EXPOSE_NATIVE_WIN32
#define GLFW_EXPOSE_NATIVE_WGL

#include "targetver.h"

#include <Windows.h>
#include <winsvc.h>  // windows services
#include <ppl.h>     // parallel for
#include <Shlwapi.h> // for PathRemoveFileSpec
#include <fileapi.h> // for GetFileTime function
#include <sys/stat.h>
#include <commctrl.h>
#include <stdio.h>
#include <tchar.h>

#include <map>
#include <deque>
#include <chrono>
#include <thread>
#include <mutex>
#include <vector>
#include <algorithm>
#include <functional>
#include <condition_variable>

#include <GL/glew.h>
#include <GL/wglew.h>
#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

#include <al.h>
#include <alc.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <OVR.h>
#include <OVR_CAPI_GL.h>

#include <stb.h>
#include <stb_image.h>

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>
#include <rapidjson/filestream.h>

extern "C"
{
    #include <libavcodec/avcodec.h>
    #include <libavformat/avformat.h>
    #include <libswscale/swscale.h>
    #include <libavresample/avresample.h>
    #include <libavutil/opt.h>
    #include <libavutil/imgutils.h>
}

extern char path[];
extern GLFWwindow* window;

GLuint loadImage(const char* filename, int* w, int* h);
