//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by RiftMediaPlayer.rc
//
#define IDI_ICON1                       101
#define IDD_DIALOG1                     102
#define IDD_DIALOGBAR                   103
#define IDB_PNG2                        104
#define IDB_GREEN                       105
#define IDB_GRE                         106
#define IDB_RED                         106
#define IDB_BITMAP1                     108
#define IDB_TIMELINE                    108
#define IDC_BUTTON1                     1001
#define IDC_OK                          1001
#define IDC_BTNPLAY                     1001
#define IDC_PROGRESS1                   1002
#define IDC_BTNSTOP                     1003
#define IDC_BTNRECENTER                 1004
#define IDC_LBLFRAME                    1005
#define IDC_SEMAPHORE                   1006
#define IDC_TIMELINE                    1007

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        109
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1008
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
