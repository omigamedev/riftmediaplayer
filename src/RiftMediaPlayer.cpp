#include "stdafx.h"
#include "RiftMediaPlayer.h"

GLuint loadImage(const char* filename, int* w, int* h)
{
    int comp;
    unsigned char* data = stbi_load(filename, w, h, &comp, 3);
    if (data == nullptr)
        return 0;

    GLuint tex;
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, *w, *h, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
    for (int y = 0; y < *h; y++)
    {
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, *h-y-1, *w, 1, GL_RGB, GL_UNSIGNED_BYTE, data + y * *w * 3);
    }
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, 0);
    return tex;
}

void createDistortionMesh(DistortionMesh& mesh, int eyeID)
{
    ovrDistortionMesh dm;
    std::vector<float> vertexData;
    std::vector<float> rData, gData, bData;
    std::vector<float> vignette;
    std::vector<GLuint> vertexIndex;
    ovrHmd_CreateDistortionMesh(hmd, hmd->EyeRenderOrder[eyeID], hmd->DefaultEyeFov[eyeID],
        ovrDistortionCap_Chromatic | ovrDistortionCap_TimeWarp, &dm);
    for (unsigned int i = 0; i < dm.VertexCount; i++)
    {
        ovrDistortionVertex& v = dm.pVertexData[i];
        vertexData.push_back(v.ScreenPosNDC.x);
        vertexData.push_back(v.ScreenPosNDC.y);
        vertexData.push_back(0);

        rData.push_back(v.TanEyeAnglesR.x);
        rData.push_back(v.TanEyeAnglesR.y);

        gData.push_back(v.TanEyeAnglesG.x);
        gData.push_back(v.TanEyeAnglesG.y);

        bData.push_back(v.TanEyeAnglesB.x);
        bData.push_back(v.TanEyeAnglesB.y);

        vignette.push_back(v.VignetteFactor);
    }
    for (unsigned int i = 0; i < dm.IndexCount; i++)
    {
        vertexIndex.push_back(dm.pIndexData[i]);
    }
    mesh.create(vertexData, rData, gData, bData, vertexIndex, vignette);
    ovrHmd_DestroyDistortionMesh(&dm);
}

bool init()
{
    printf("Graphic Vendor  : %s\n", glGetString(GL_VENDOR));
    printf("Graphic Renderer: %s\n", glGetString(GL_RENDERER));
    printf("GL  : %s\n", glGetString(GL_VERSION));
    printf("GLSL: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    printf("GLFW: %s\n", glfwGetVersionString());
    printf("GLEW: %s\n", glewGetString(GLEW_VERSION));
    printf("OVR : %s\n", ovr_GetVersionString());
    printf("AL  : %s\n", alGetString(AL_VERSION));
    printf("Audio Renderer: %s\n", alGetString(AL_RENDERER));

    if (hmd)
    {
        printf("Rift Firmware : %d.%d\n", hmd->FirmwareMajor, hmd->FirmwareMinor);
        printf("\n--------------------\n\n");
    }

    // Init OpenGL state
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);

    // Setup camera and projection
    camera_rot = glm::vec2();
    //proj = glm::perspective(2.19686294f, eye_aspect, 0.1f, 100.f);
    proj = glm::perspective(glm::radians(126.f), eye_aspect, 0.1f, 100.f);

    // Load shaders
    sm.loadShader("yuv420");
    sm.loadShader("barrel");
    sm.loadShader("color-flat");
    sm.loadShader("texture-flat");
    sm.loadShader("distortion");
    // Create distortion framebuffer
    rtt.create(eye_w * 2, eye_h);

    player_left.onComplete = [&](){
        paused = true;
        //firstFrame = true;
        //SetDlgItemText(hDialog, IDC_BTNPLAY, paused ? L"Play" : L"Pause");
        //EnableWindow(GetDlgItem(hDialog, IDC_BTNPLAY), FALSE);
    };

    switch (file.media)
    {
    case MediaType::IMAGE:
        if (file.type == StereoType::STEREO)
        {
            int w, h;
            image_stereo[0] = loadImage(file.left.c_str(), &w, &h);
            image_stereo[1] = loadImage(file.right.c_str(), &w, &h);
        }
        else
        {
            int w, h;
            stat(file.left.c_str(), &file.info);
            image_stereo[0] = image_stereo[1] = loadImage(file.left.c_str(), &w, &h);
            if (file.type == StereoType::AUTODETECT)
                file.type = (w == h) ? StereoType::BOTTOM_UP : StereoType::SINGLE;

            file.pollThread = std::thread([]{
                while (active)
                {
                    struct stat info;
                    stat(file.left.c_str(), &info);
                    if (info.st_mtime > file.info.st_mtime)
                    {
                        FILE* fp = nullptr;
                        if (fp = fopen(file.left.c_str(), "r"))
                        {
                            char buf[16];
                            fseek(fp, 0, SEEK_END);
                            long len = ftell(fp);
                            if (len > 128)
                            {
                                file.changed = true;
                                file.info = info;
                                std::printf("len %d\n", (int)len);
                            }
                            fclose(fp);
                        }
                    }
                    Sleep(1000);
                }
            });
        }
        break;

    case MediaType::VIDEO:
        if (file.type == StereoType::STEREO)
        {
            if (!player_left.load(file.left.c_str()))
                printf("Cannot load the left stream of the video.\n");
            if (!player_right.load(file.right.c_str()))
                printf("Cannot load the right stream of the video.\n");
            image_stereo[0] = player_left.video_tex;
            image_stereo[1] = player_right.video_tex;
        }
        else
        {
            if (!player_left.load(file.left.c_str()))
            {
                printf("Cannot load the video.\n");
            }
            else
            {
                int w = player_left.width;
                int h = player_left.height;
                if (file.type == StereoType::AUTODETECT)
                    file.type = (w == h) ? StereoType::BOTTOM_UP : StereoType::SINGLE;
            }
            image_stereo[0] = image_stereo[1] = player_left.video_tex;
        }
        break;
    }


    // Init meshes
    stereoPlane.create(2, 2);
    stereoPlane.color = glm::vec4(0, 0, 1, 1);
    stereoPlane.model = glm::mat4();
    stereoPlane.material.diffuse_tex = rtt.getTextureID();
    
    cube.create(1);
    sphere.create(100, 32, 32);

    // Init OpenAL scene
    alListener3f(AL_POSITION, 0, 0, 0);

    SetFocus(glfwGetWin32Window(window));
    SetActiveWindow(glfwGetWin32Window(window));

    return true;
}

void renderEye(int eyeID)
{
    RenderTexture& rtt = EyeTexture[eyeID];

    float eye_x = eyeID ? 0.5f : 0.0f;
    int eye_sgn = eyeID ? 1 : -1;

    ovrEyeType eye = hmd->EyeRenderOrder[eyeID];
    ovrFovPort fov = hmd->DefaultEyeFov[eyeID];
    ovrEyeRenderDesc eyeDesc = ovrHmd_GetRenderDesc(hmd, eye, fov);

    ovrPosef pose = ovrHmd_GetHmdPosePerEye(hmd, eye);

    static float    BodyYaw(glm::radians(90.f));
    static OVR::Vector3f HeadPos(0, 0, 0);

    //BodyYaw += 0.003f;

    // Get view and projection matrices
    OVR::Matrix4f rollPitchYaw = OVR::Matrix4f::RotationY(BodyYaw);
    OVR::Matrix4f finalRollPitchYaw = rollPitchYaw * OVR::Matrix4f(pose.Orientation);
    OVR::Vector3f finalUp = finalRollPitchYaw.Transform(OVR::Vector3f(0, 1, 0));
    OVR::Vector3f finalForward = finalRollPitchYaw.Transform(OVR::Vector3f(0, 0, -1));
    OVR::Vector3f shiftedEyePos = HeadPos + rollPitchYaw.Transform(pose.Position);
    OVR::Matrix4f view = OVR::Matrix4f::LookAtRH(shiftedEyePos, shiftedEyePos + finalForward, finalUp);
    OVR::Matrix4f proj = ovrMatrix4f_Projection(eyeDesc.Fov, 0.01f, 10000.0f, true);

    // Transform to OpenGL compatible matrix
    glm::mat4 eyeProj = glm::make_mat4(&proj.Transposed().M[0][0]);
    glm::mat4 modelview = glm::make_mat4(&view.Transposed().M[0][0]);

    sphere.material.diffuse_tex = stereo ? image_stereo[eyeID] : image_stereo[0];
    cube.material.diffuse_tex = sphere.material.diffuse_tex;
    glm::mat4 stereoMat;
    if (file.type == StereoType::BOTTOM_UP)
    {
        stereoMat = glm::translate(glm::vec3(0, (stereo ? eyeID : 0) ? 0 : 0.5, 0)) *
            glm::scale(glm::vec3(1, .5, 1));
    }

    // Draw to texture
    rtt.bindFramebuffer();
    glViewport(0, 0, rtt.getWidth(), rtt.getHeight());
    rtt.clear(glm::vec4(1, eye, 1, 1));
    sm.useShader(file.media == MediaType::VIDEO ? "yuv420" : "texture-flat");
    sm.uniformMatrix4f("texmat", stereoMat);
    sm.uniformMatrix4f("proj", eyeProj);
    sm.uniformMatrix4f("modelview", modelview);
    sm.uniform1i("tex0", TEX_DIFFUSE);
    sphere.draw(GL_QUADS);
    //cube.draw(GL_QUADS);
    rtt.unbindFramebuffer();
}

void renderFrames()
{
    static int decode = 0;
    static int convert = 0;
    static int upload = 0;
    static char str[256];

    static int frames = 0;
    static int video_frames = 0;
    static double start = glfwGetTime();
    static double time_decode = glfwGetTime();
    static double time_fps = glfwGetTime();
    double sec = 1.0 / 75.0;
    double stop = glfwGetTime();
    double ms = stop - start;
    start = stop;

    time_decode += ms;
    time_fps += ms;

    if (player_left.format && !vsync)
    {
        sec = (double)player_left.video_stream->avg_frame_rate.den /
            (double)player_left.video_stream->avg_frame_rate.num;
    }

    // Every second
    if (time_fps > 1.0)
    {

        if (fullscreen)
        {
            // Set dialog title
            sprintf(str, "%s - %d/%d fps", "RiftMediaPlayer Controls", video_frames, frames);
            SetWindowTextA(hDialog, str);
        }
        else
        {
            // Set surface title
            sprintf(str, "%s - %d/%d fps", title, video_frames, frames);
            glfwSetWindowTitle(window, str);
            // Set dialog title
            sprintf(str, "%s - %d/%d fps", "RiftMediaPlayer Controls", video_frames, frames);
            SetWindowTextA(hDialog, str);
        }

        //printf("Playing at %d/%dfps\n", video_frames, frames);
        // Update progress bar
        int progress = (int)(player_left.getProgress() * 100);
        SendMessage(hProgress, PBM_SETPOS, progress, 0);

        sprintf(str, "Frame %05d", player_left.video_played_frames);
        SetWindowTextA(hLblFrame, str);

        frames = 0;
        video_frames = 0;
        time_fps -= 1;
    }

    if (!paused)
    {
        player_left.updateAudio();
        player_right.updateAudio();
    }

    bool decoded = false;
    // Every video frame
    if (time_decode > sec || firstFrame)
    {
        if (!paused || firstFrame)
        {
            bool done1 = player_left.getNextFrame();
            bool done2 = player_right.getNextFrame();
            if (done1)
            {
                firstFrame = false;
                video_frames++;
            }

            if (player_left.video_played_frames == 3750)
                SendDlgItemMessage(hDialog, IDC_SEMAPHORE, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hBmpGreen);

            decoded = true;
        }

        time_decode -= sec;
    }


    {
//        std::unique_lock<std::mutex> lock(render_mutex);
        renderEye(0);
        renderEye(1);
    }

    if (decoded)
        player_left.getNextFrame_post();

    frames++;
}

void renderFrameThread()
{
    BOOL ret = wglMakeCurrent(hDC, hFrameRC);
    while (active)
    {
        renderFrames();
    }
}

void renderToRift()
{
    //std::unique_lock<std::mutex> lock(render_mutex);
    if (hmd)
    {
        ovrGLTexture eyeTexture[2]; // Gather data for eye textures 
        for (int eye = 0; eye < 2; eye++)
        {
            EyeRenderViewport[eye].Pos = OVR::Vector2i(0, 0);
            EyeRenderViewport[eye].Size = idealSize;
            eyeTexture[eye].OGL.Header.API = ovrRenderAPI_OpenGL;
            eyeTexture[eye].OGL.Header.TextureSize = idealSize;
            eyeTexture[eye].OGL.Header.RenderViewport = EyeRenderViewport[eye];
            eyeTexture[eye].OGL.TexId = EyeTexture[eye].getTextureID();
        }

        ovrHmd_BeginFrame(hmd, 0);
        renderFrames();
        ovrVector3f useHmdToEyeViewOffset[2] = { EyeRenderDesc[0].HmdToEyeViewOffset,
            EyeRenderDesc[1].HmdToEyeViewOffset };
        ovrHmd_GetEyePoses(hmd, 0, useHmdToEyeViewOffset, EyeRenderPose, NULL);
        ovrHmd_EndFrame(hmd, EyeRenderPose, &eyeTexture[0].Texture);
    }
}

void window_resize(GLFWwindow* window, int w, int h)
{
    g_width = w;
    g_height = h;
}

void window_key(GLFWwindow* window,
    int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
    {
        switch (key)
        {
        case GLFW_KEY_ESCAPE:
            active = false;
            break;
        case GLFW_KEY_1:
            eye_w = 960 / 4;
            eye_h = 540 / 4;
            rtt.create(eye_w * 2, eye_h);
            stereoPlane.material.diffuse_tex = rtt.getTextureID();
            break;
        case GLFW_KEY_2:
            eye_w = 960 / 2;
            eye_h = 540 / 2;
            rtt.create(eye_w * 2, eye_h);
            stereoPlane.material.diffuse_tex = rtt.getTextureID();
            break;
        case GLFW_KEY_3:
            eye_w = 960 * 1;
            eye_h = 540 * 1;
            rtt.create(eye_w * 2, eye_h);
            stereoPlane.material.diffuse_tex = rtt.getTextureID();
            break;
        case GLFW_KEY_4:
            eye_w = 960 * 4;
            eye_h = 540 * 4;
            rtt.create(eye_w * 2, eye_h);
            stereoPlane.material.diffuse_tex = rtt.getTextureID();
            break;
        case GLFW_KEY_V:
            vsync = !vsync;
            //glfwSwapInterval(vsync ? 1 : 0);
            break;
        case GLFW_KEY_C:
            chromaEnabled = !chromaEnabled;
            break;
        case GLFW_KEY_P:
            break;
        case GLFW_KEY_S:
            // Toggle stereo view
            stereo = !stereo;
            break;
        case GLFW_KEY_R:
            // Rewind the video
            firstFrame = false;
            player_left.rewind();
            player_right.rewind();
            break;
        case GLFW_KEY_SPACE:
            paused = !paused;
            break;
        case GLFW_KEY_ENTER:
            ovrHmd_RecenterPose(hmd);
            break;
        case GLFW_KEY_F11:
            // Toggle fullscreen
            firstFrame = false;
            fullscreen = !fullscreen;
            player_left.close();
            player_right.close();
            if (!createWindow(fullscreen))
                exit(-1);
            init();
            break;
        default:
            break;
        }
    }
}

void window_cursor(GLFWwindow* window, double x, double y)
{
    glm::vec2 oldPos(Cursor::pos);
    Cursor::pos = glm::vec2(x, y);
    mouseLeft.dragDelta = mouseLeft.down ? Cursor::pos - mouseLeft.dragStart : glm::vec2();
    mouseRight.dragDelta = mouseRight.down ? Cursor::pos - mouseRight.dragStart : glm::vec2();
    if (mouseLeft.down)
        camera_rot += glm::radians(Cursor::pos - oldPos);
}

void window_button(GLFWwindow* window, int button, int action, int mods)
{
    switch (button)
    {
    case GLFW_MOUSE_BUTTON_LEFT:
        mouseLeft.down = (action == GLFW_PRESS);
        mouseLeft.dragStart = Cursor::pos;
        break;
    case GLFW_MOUSE_BUTTON_RIGHT:
        mouseRight.down = (action == GLFW_PRESS);
        mouseRight.dragStart = Cursor::pos;
        break;
    }
}

void window_close(GLFWwindow* window)
{
    active = false;
}

bool createWindow(bool fs)
{
    // Select appropriate monitor
    int monitors_count;
    GLFWmonitor** monitors = glfwGetMonitors(&monitors_count);
    GLFWmonitor* selectedMonitor = nullptr;
    GLFWmonitor* guiMonitor = monitors[monitors_count - 1];
    char dev[33];
    
    // Destroy any existing window
    if (window)
        glfwDestroyWindow(window);

    g_GUI_x = g_GUI_y = 0;

    //glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_REFRESH_RATE, 75);
    glfwWindowHint(GLFW_AUTO_ICONIFY, 0);
    if (!(window = glfwCreateWindow(g_width, g_height, title,
        fs ? selectedMonitor : nullptr, nullptr)))
    {
        return false;
    }
    glfwHideWindow(window);
    hWnd = glfwGetWin32Window(window);
    hDC = GetDC(hWnd);

    // Set Icon
    SendMessage(glfwGetWin32Window(window), WM_SETICON, ICON_SMALL, 
        (LPARAM)LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON1)));

    fullscreen = fs;

    glfwSetWindowCloseCallback(window, window_close);
    glfwSetWindowSizeCallback(window, window_resize);
    glfwSetKeyCallback(window, window_key);
    glfwSetCursorPosCallback(window, window_cursor);
    glfwSetMouseButtonCallback(window, window_button);
    glfwMakeContextCurrent(window);
    glfwSwapInterval(0);

    // Init OpenGL extensions
    glewInit();
    //wglSwapIntervalEXT(-1);

    return true;
}

bool oalInit()
{
    oalDevice = alcOpenDevice(nullptr);
    if (!oalDevice)
        return false;
    oalContext = alcCreateContext(oalDevice, nullptr);
    if (!oalContext)
        return false;
    alcMakeContextCurrent(oalContext);
    return true;
}

void oalDestroy()
{
    alcMakeContextCurrent(nullptr);
    alcDestroyContext(oalContext);
    alcCloseDevice(oalDevice);
}

INT_PTR CALLBACK MainDialog(HWND hDlg, UINT msg, WPARAM wp, LPARAM lp)
{
    HWND btnPlay = GetDlgItem(hDlg, IDC_BTNPLAY);
    switch (msg)
    {
    case WM_INITDIALOG:
        hBmpRed = (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_RED), IMAGE_BITMAP, 0, 0, 0);
        hBmpGreen = (HBITMAP)LoadImage(hInst, MAKEINTRESOURCE(IDB_GREEN), IMAGE_BITMAP, 0, 0, 0);
        SendDlgItemMessage(hDlg, IDC_SEMAPHORE, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM)hBmpRed);
        return TRUE;
    case WM_COMMAND:
        switch (wp)
        {
        case IDC_BTNPLAY:
            paused = !paused;
            SetDlgItemText(hDlg, IDC_BTNPLAY, paused ? L"Play" : L"Pause");
            return TRUE;
        case IDC_BTNSTOP:
            player_left.rewind();
            player_right.rewind();
            paused = true;
            firstFrame = true;
            break;
        case IDC_BTNRECENTER:
            ovrHmd_RecenterPose(hmd);
            break;
        }
        break;
    case WM_CLOSE:
        EndDialog(hDlg, 0);
        active = false;
        return TRUE;
    }
    return FALSE;
}

int WINAPI WinMain(_In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, 
    _In_ LPSTR lpCmdLine, _In_ int nShowCmd)
{
    hInst = hInstance;
    LPWSTR *szArgList{ nullptr };
    int argc{ 0 };
    char** argv{ nullptr };

    szArgList = CommandLineToArgvW(GetCommandLine(), &argc);
    if (szArgList == NULL)
    {
        MessageBox(NULL, L"Unable to parse command line", L"Error", MB_OK);
        return 10;
    }

    argv = new char*[argc + 1];
    for (int i = 0; i < argc; i++)
    {
        auto len = wcslen(szArgList[i]) + 1;
        argv[i] = new char[len];
        wcstombs(argv[i], szArgList[i], len);
    }

    LocalFree(szArgList);

    return main(argc, argv);
}

int main(int argc, char* argv[])
{
    //atexit([](){ system("pause"); });

    // Retrieve the current directory
    strcpy(path, argv[0]);
    PathRemoveFileSpecA(path);

    if (argc > 1)
    {
        printf("Opening %s\n", argv[1]);
        char* ext = PathFindExtensionA(argv[1]);
        if (strcmp(ext, ".rmp") == 0)
        {
            if (!file.LoadRMP(argv[1]))
                printf("Failed opening file\n");
        }
        if (strcmp(ext, ".png") == 0 || strcmp(ext, ".jpg") == 0)
        {
            file.left = argv[1];
            file.media = MediaType::IMAGE;
            file.type = StereoType::AUTODETECT;
            if (argc > 2)
            {
                char* ext2 = PathFindExtensionA(argv[2]);
                if (strcmp(ext2, ".png") == 0 || strcmp(ext2, ".jpg") == 0)
                {
                    file.right = argv[2];
                    file.type = StereoType::STEREO;
                }
            }
        }
        if (strcmp(ext, ".mp4") == 0)
        {
            file.left = argv[1];
            file.media = MediaType::VIDEO;
            file.type = StereoType::AUTODETECT;
            if (argc > 2)
            {
                char* ext2 = PathFindExtensionA(argv[2]);
                if (strcmp(ext2, ".mp4") == 0)
                {
                    file.right = argv[2];
                    file.type = StereoType::STEREO;
                }
            }
        }
    }
    else
    {
        if (!file.LoadRMP(R"(C:\Users\omar\Desktop\PlayerSync\playersync25.rmp)"))
            printf("Failed opening file\n");
    }
    

    // Init avlib
    av_register_all();
    avcodec_register_all();
    av_log_set_level(AV_LOG_QUIET);
    
    ovr_Initialize();

    if (!glfwInit())
    {
        printf("GLFW init error\n");
        return -1;
    }

    // Create the window, fullscreen mode if shell opened
#ifdef _DEBUG
    if (!createWindow(false))
#else
    if (!createWindow(false))
#endif
    {
        printf("Window creation error\n");
        return -1;
    }

    // Init decoder GL context
    player_left.gl_ctx = wglCreateContext(GetDC(glfwGetWin32Window(window)));
    wglShareLists(glfwGetWGLContext(window), player_left.gl_ctx);

    // Init eyes texture renderer GL context
    hFrameRC = wglCreateContext(GetDC(glfwGetWin32Window(window)));
    wglShareLists(glfwGetWGLContext(window), hFrameRC);

    // Init OVR
    hmd = ovrHmd_Create(0);
    if (hmd)
    {
        ovrHmd_AttachToWindow(hmd, hWnd, nullptr, nullptr);
        ovrHmd_SetEnabledCaps(hmd, ovrHmdCap_LowPersistence | ovrHmdCap_DynamicPrediction | ovrHmdCap_NoMirrorToWindow);
        ovrHmd_ConfigureTracking(hmd, ovrTrackingCap_Orientation | ovrTrackingCap_MagYawCorrection |
            ovrTrackingCap_Position, 0);

        // Make the eye render buffers (caution if actual size < requested due to HW limits). 
        for (int eye = 0; eye < 2; eye++)
        {
            idealSize = ovrHmd_GetFovTextureSize(hmd, (ovrEyeType)eye, hmd->DefaultEyeFov[eye], 4.0f);
            EyeTexture[0].create(idealSize.w, idealSize.h);
            EyeTexture[1].create(idealSize.w, idealSize.h);
            EyeRenderViewport[eye].Pos = OVR::Vector2i(0, 0);
            EyeRenderViewport[eye].Size = idealSize;
        }

        ovrGLConfig glconf;
        glconf.OGL.Header.API = ovrRenderAPI_OpenGL;
        glconf.OGL.Header.BackBufferSize = OVR::Sizei(hmd->Resolution.w, hmd->Resolution.h);
        glconf.OGL.Header.Multisample = 1;
        glconf.OGL.DC = hDC;
        glconf.OGL.Window = hWnd;

        ovrHmd_ConfigureRendering(hmd, &glconf.Config, ovrDistortionCap_Chromatic | ovrDistortionCap_Vignette |
            ovrDistortionCap_TimeWarp | ovrDistortionCap_Overdrive, hmd->DefaultEyeFov, EyeRenderDesc);
    }
    else
    {
        printf("Could not find any Oculus Rift device\n");
        system("pause");
        return -1;
    }
    
    // Init OpenAL
    if (!oalInit())
    {
        printf("OpenAL init error\n");
        return -1;
    }

    // Init GUI
    hDialog = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG1),
        NULL, (DLGPROC)MainDialog);
    SendMessage(hDialog, WM_SETICON, ICON_SMALL, (LPARAM)LoadIcon(hInst, MAKEINTRESOURCE(IDI_ICON1)));
    ShowWindow(hDialog, SW_NORMAL);
    SetWindowPos(hDialog, NULL, g_GUI_x + 100, g_GUI_y + 100, 0, 0, SWP_NOSIZE);

    hProgress = GetDlgItem(hDialog, IDC_PROGRESS1);
    SendMessage(hProgress, PBM_SETRANGE, 0, MAKELPARAM(0, 100));
    SendMessage(hProgress, PBM_SETPOS, 50, 0);

    hLblFrame = GetDlgItem(hDialog, IDC_LBLFRAME);

#ifdef _DEBUG
    Console::RedirectIOToConsole();
    hConsole = GetConsoleWindow();
    SetWindowPos(hConsole, NULL, g_GUI_x + 100, g_GUI_y + 300, 0, 0, SWP_NOSIZE);

    RECT r;
    GetWindowRect(glfwGetWin32Window(window), &r);
    //SetWindowPos(glfwGetWin32Window(window), NULL, g_GUI_x + r.left, g_GUI_y + r.top, 0, 0, SWP_NOSIZE);
#endif

    // Move the cursor near the GUI, because GLFW centers it on the fullscreen
    SetCursorPos(g_GUI_x + 100, g_GUI_y + 100);

    // Init the scene
    if (!init())
    {
        printf("Init scene error\n");
        return -1;
    }

    // Start render thread
    //render_thread = std::thread(renderFrameThread);

    ovrHmd_DismissHSWDisplay(hmd);

    // Main loop
    double maxms = 1.0 / 75.0;
    while (!glfwWindowShouldClose(window) && active)
    {
        double start = glfwGetTime();

        if (file.changed)
        {
            int w, h;
            image_stereo[0] = image_stereo[1] = loadImage(file.left.c_str(), &w, &h);
            if (image_stereo[0])
            {
                if (file.type == StereoType::AUTODETECT)
                    file.type = (w == h) ? StereoType::BOTTOM_UP : StereoType::SINGLE;
                file.changed = false;
            }
            std::printf("RELOAD\n");
        }

        renderToRift();
        //glfwSwapBuffers(window);
        glfwPollEvents();
        double diff = glfwGetTime() - start;
        int ms = static_cast<int>((diff) * 1000.0);
    }
    
    active = false;
    player_left.close();
    player_right.close();

    wglDeleteContext(player_left.gl_ctx);
    wglDeleteContext(hFrameRC);

    // Close OVR
    if (hmd)
        ovrHmd_Destroy(hmd);
    ovr_Shutdown();

    glfwMakeContextCurrent(nullptr);
    glfwDestroyWindow(window);
    glfwTerminate();

    oalDestroy();


    return 0;
}

