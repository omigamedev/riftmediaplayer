#pragma once

#define VP_IGNORE_AUDIO false
#define VP_PBOS 8

class VideoPlayer
{
    void decodeThread();
    void decodeFrame();

public:
    std::string      path;
    AVFormatContext* format;

    AVCodec*        video_decoder;
    AVCodecContext* video_context;
    AVStream*       video_stream;
    SwsContext*     video_convert_ctx;
    AVFrame*        video_frame;
    int             video_played_frames;
    int             video_stream_index;
    GLuint          video_tex;
    int             video_current_pbo;
    GLuint          video_pbo_last;
    std::deque<GLuint> video_pbos_empty;
    std::deque<GLuint> video_pbos_ready;
    std::deque<struct AVPacket> video_packets;
    int rgb_size;
    int width;
    int height;
    int width2;
    int height2;
    HGLRC gl_ctx;

    AVCodec*        audio_decoder;
    AVCodecContext* audio_context;
    AVStream*       audio_stream;
    AVFrame*        audio_frame;
    int             audio_stream_index;
    int             audio_played_samples;
    ALuint          audio_source;
    AVAudioResampleContext* audio_resample;
    std::deque<ALuint> audio_buffer_queue;
    std::mutex      audio_buffer_mutex;

    int time_decode;
    int time_convert;
    int time_upload;

    VideoPlayer();
    ~VideoPlayer();

    bool load(const char* filename, bool ignore_audio = false);
    void updateAudio();
    bool getNextFrame();
    void getNextFrame_post();
    float getProgress();
    void rewind();
    void close();

    std::function<void(void)> onComplete;
    std::function<void(void)> onLoad;

    bool active;
    std::thread read_thread;
    std::mutex read_mutex;
    std::condition_variable read_cv;
};

