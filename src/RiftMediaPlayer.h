#pragma once

#include "Shader.h"
#include "Primitive.h"
#include "ShaderManager.h"
#include "RenderTexture.h"
#include "VideoPlayer.h"
#include "Console.h"
#include "resource.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb.h>
#include <stb_image.h>

bool createWindow(bool fs);
bool init();
void renderEye(int eyeID);
void renderToRift();
void window_close(GLFWwindow* window);
void window_resize(GLFWwindow* window, int w, int h);
void window_key(GLFWwindow* window, int key, int scancode, int action, int mods);
void window_cursor(GLFWwindow* window, double x, double y);
void window_button(GLFWwindow* window, int button, int action, int mods);
bool oalInit();
void oalDestroy();
int main(int argc, char* argv[]);

struct Cursor
{
    static glm::vec2 pos;
    glm::vec2 dragStart;
    glm::vec2 dragDelta;
    bool dragging;
    bool down;
};
glm::vec2 Cursor::pos;

enum class StereoType
{
    SINGLE,
    BOTTOM_UP,
    STEREO,
    AUTODETECT
};

enum class MediaType
{
    IMAGE,
    VIDEO
};

/*
Example SINGLE and BOTTOM_UP
{
  "title": "Title",
  "media": "video" / "image",
  "type": "single" / "bottom-up",
  "path": "file.mp4"
}
Example STEREO
{
  "title": "Title",
  "media": "video" / "image",
  "type": "stereo",
  "left": "left.mp4",
  "right": "right.mp4"
}
*/
struct MediaFile
{
    bool LoadRMP(const char* filename)
    {
        FILE* fp = fopen(filename, "r");
        if (!fp)
        {
            printf("Error opening %s\n", filename);
            return false;
        }
        static char path[256];
        strcpy(path, filename);
        PathRemoveFileSpecA(path);
        std::string base = path;

        file = filename;
        rapidjson::FileStream fs(fp);
        rapidjson::Document d;
        d.ParseStream<0>(fs);
        title = d["title"].GetString();
        if (strcmp("bottom-up", d["type"].GetString()) == 0)
        {
            type = StereoType::BOTTOM_UP;
            left = base + "\\" + d["path"].GetString();
        }
        if (strcmp("single", d["type"].GetString()) == 0)
        {
            type = StereoType::SINGLE;
            left = base + "\\" + d["path"].GetString();
        }
        if (strcmp("stereo", d["type"].GetString()) == 0)
        {
            type = StereoType::STEREO;
            left = base + "\\" + d["left"].GetString();
            right = base + "\\" + d["right"].GetString();
        }
        if (strcmp("image", d["media"].GetString()) == 0)
        {
            media = MediaType::IMAGE;
        }
        if (strcmp("video", d["media"].GetString()) == 0)
        {
            media = MediaType::VIDEO;
        }
        
        fclose(fp);
        
        printf("Media title: %s\n", title.c_str());
        return true;
    }

    ~MediaFile()
    {
        pollThread.detach();
    }

    MediaType media;
    StereoType type;
    std::string file;
    std::string left;
    std::string right;
    std::string title;
    struct stat info;
    std::thread pollThread;
    bool changed{ false };
};

#define RIFT_WIDTH 1920
#define RIFT_HEIGHT 1080
#define EYE_WIDTH (RIFT_WIDTH/2)
#define EYE_HEIGHT RIFT_HEIGHT

int eye_w = EYE_WIDTH * 2;
int eye_h = EYE_HEIGHT * 2;
float eye_aspect = (float)eye_w / (float)eye_h;
const char title[] = "Rift Media Player";
char path[256] = "";
MediaFile file;

int g_width = RIFT_WIDTH;
int g_height = RIFT_HEIGHT;

bool active = true;
bool fullscreen = false;
bool stereo = true;
bool paused = true;
bool firstFrame = true;
bool chromaEnabled = true;
bool vsync = false; // Force 75Hz

GLFWwindow* window = nullptr;
HWND hDialog;
HWND hProgress;
HWND hLblFrame;
HWND hConsole;
HINSTANCE hInst;
HBITMAP hBmpRed;
HBITMAP hBmpGreen;

ovrEyeRenderDesc EyeRenderDesc[2]; // Description of the VR.
ovrRecti EyeRenderViewport[2];     // Useful to remember when varying resolution
ovrPosef EyeRenderPose[2];         // Useful to remember where the rendered eye originated
RenderTexture EyeTexture[2];
OVR::Sizei idealSize;
HWND hWnd;
HDC hDC;
HGLRC hFrameRC;
std::thread render_thread;
std::mutex render_mutex;
std::condition_variable render_cv;

// GUI x position shift
int g_GUI_x;
int g_GUI_y;

ALCdevice* oalDevice = nullptr;
ALCcontext* oalContext = nullptr;

glm::mat4 proj;
glm::mat4 vrcamera;
glm::vec2 camera_rot;

ovrHmd hmd;
ovrFrameTiming timer;

DistortionMesh distMesh[2];
Cursor mouseLeft;
Cursor mouseRight;
ShaderManager sm;
RenderTexture rtt;
Plane stereoPlane;
Sphere sphere;
Cube cube;
GLuint image_stereo[2];
VideoPlayer player_left;
VideoPlayer player_right;
